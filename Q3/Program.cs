﻿using System;
using System.Linq;
namespace Q3
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] arr1 = { "A", "B", "C", "D", "E" };
            string[] arr2 = { "B", "D", "E", "F", "G", "H", "I" };

            var result = arr1.Intersect(arr2);

            Console.WriteLine($"Array 1 : {String.Join(",", arr1)}");
            Console.WriteLine($"Array 2 : {String.Join(",", arr2)}");
            Console.WriteLine($"Result : {String.Join(",", result)}");
        }
    }
}
