﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Q2.Helpers
{
    public class Shift
    {
        public TimeSpan Start { get; set; }
        public TimeSpan End { get; set; }
    }
}
