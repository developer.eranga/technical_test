﻿using Q2.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Q2.Interfaces
{
   public interface IShiftChecker
    {
        bool IsActiveShift( DateTime dateTime);
    }
}
