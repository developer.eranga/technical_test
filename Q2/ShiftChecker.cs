﻿using Q2.Helpers;
using Q2.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Q2
{
    public class ShiftChecker : IShiftChecker
    {
        private readonly Shift _shift;
        public ShiftChecker(Shift shift) {
            _shift = shift;
        }
        public bool IsActiveShift(  DateTime dateTime)
        {
            if (_shift.Start <= dateTime.TimeOfDay && _shift.End >= dateTime.TimeOfDay) {
                return true;
            }

            return false;
        }
    }
}
