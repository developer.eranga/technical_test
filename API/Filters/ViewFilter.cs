﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Filters
{
    public class ViewFilter:Attribute
    {
        public bool ShowInView { get; set; }

        public ViewFilter() => ShowInView = false;

    }
}
