﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Test.Filters;
using Test.Generic;

namespace Test.Storage
{
    public class Post:IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [ViewFilter(ShowInView =false)]
        public int Id { get; set; }

        [Display(Name ="Title")]
        [ViewFilter(ShowInView =true)]
        public string Title { get; set; }

        [Display(Name ="Image Url")]
        [ViewFilter(ShowInView = true)]
        public string ImageUrl { get; set; }

        [Display(Name ="Created Date")]
        [ViewFilter(ShowInView = true)]
        public DateTime CreatedDate { get; set; }

        [Display(Name ="Total Links")]
        [ViewFilter(ShowInView = false)]
        public int TotalLinks { get; set; }

        public override string ToString()
        {
            return Title;
        }
    }
}
