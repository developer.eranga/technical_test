﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Test.Filters;
using Test.Generic;

namespace Test.Storage
{
    public class Person : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [ViewFilter(ShowInView = false)]
        public int Id { get; set; }

        [ViewFilter(ShowInView = true)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [ViewFilter(ShowInView = true)]
        public string LastName { get; set; }

        [Display(Name = "Email")]
        [ViewFilter(ShowInView = true)]
        public string Email { get; set; }

        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }
    }
}
