﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Test.Filters;

namespace Test.Generic
{
    public class ViewBaseController<T>: Controller where T : class, IEntity
    {
        public IActionResult Index() {           
            return View("GenericIndex", Activator.CreateInstance<T>());
        }
    }
}
