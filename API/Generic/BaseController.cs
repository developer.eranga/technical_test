﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Test.Storage;

namespace Test.Generic
{
    [Route("api/[controller]")]
    public class BaseController<T> : ControllerBase where T : class, IEntity
    {
        private readonly DataContext _context;

        public BaseController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<T> GetAll()
        {
            return _context.Set<T>()?.ToList()??null;
        }

        [HttpGet("{id}")]
        public T Get(int id)
        {
            return _context.Set<T>().FirstOrDefault(x => x.Id == id);
        }

        [HttpPost]
        public T Save([FromBody] T entity)
        {

            _context.Set<T>().Add(entity);
            _context.SaveChanges();
            return entity;
        }

        [HttpPut]
        public T Update([FromBody] T entity)
        {
            _context.Set<T>().Update(entity);
            _context.SaveChanges();
            return entity;
        }
    }
}
