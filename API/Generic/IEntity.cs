﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Generic
{
  public  interface IEntity
    {
        int Id { get; set; }
    }
}
