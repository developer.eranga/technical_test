using NUnit.Framework;
using NUnit.Framework.Internal.Execution;
using Q2;
using Q2.Helpers;
using Q2.Interfaces;

namespace UnitTests
{
    public class UnitTestQ2

    {

        IShiftChecker shiftChecker;
        [SetUp]
        public void Setup()
        {
            var shift = new Shift()
            {

                Start = new System.TimeSpan(0, 10, 00, 00),
                End = new System.TimeSpan(0, 18, 0, 0)
            };

            shiftChecker = new ShiftChecker(shift);
        }

        [Test]
        public void BeforeStart()
        {
            Assert.IsFalse(shiftChecker.IsActiveShift(new System.DateTime(2020, 1, 1, 9, 59, 59)));

        }
        [Test]
        public void OnStart()
        {
            Assert.IsTrue(shiftChecker.IsActiveShift(new System.DateTime(2020, 1, 1, 10, 00, 0)));
        }

        [Test]
        public void AfterStart()
        {
            Assert.IsTrue(shiftChecker.IsActiveShift(new System.DateTime(2020, 1, 1, 10, 1, 0)));
        }

        [Test]
        public void Mid()
        {
            Assert.IsTrue(shiftChecker.IsActiveShift(new System.DateTime(2020, 1, 1, 13, 00, 0)));
        }

        [Test]
        public void BeforeEnd()
        {
            Assert.IsTrue(shiftChecker.IsActiveShift(new System.DateTime(2020, 1, 1, 17, 59, 59)));
        }

        [Test]
        public void OnEnd()
        {
            Assert.IsTrue(shiftChecker.IsActiveShift(new System.DateTime(2020, 1, 1, 18, 00, 0)));
        }

        [Test]
        public void AfterEnd()
        {
            Assert.IsFalse(shiftChecker.IsActiveShift(new System.DateTime(2020, 1, 1, 18, 1, 0)));
        }
    }
}